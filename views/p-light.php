                    <?php 
                        $posts = get_ci_value("post_package");
                        $addons = get_ci_value("addon_package");
                        $packages = get_ci_value("packages");
                        ?>

                    <?php if(!empty($packages)){?>
                <div class="row text-center">
                <?php
                foreach ($packages as $key => $row) {

                    $file_type = ["photo" => __("Photo"), "video" => __("Video")];
                    $cloud_import = ["google_drive" => __("Google Drive"), "dropbox" => __("Dropbox"), "one_drive" => __("One Drive")];

                    if( !isset($row->permissions['file_manager_photo']) ) unset($file_type["photo"]);
                    if( !isset($row->permissions['file_manager_video']) ) unset($file_type["video"]);

                    if( !isset($row->permissions['file_manager_google_drive']) ) unset($cloud_import["google_drive"]);
                    if( !isset($row->permissions['file_manager_dropbox']) ) unset($cloud_import["dropbox"]);
                    if( !isset($row->permissions['file_manager_onedrive']) ) unset($cloud_import["one_drive"]);

                    if(!empty($file_type)){
                        $file_type = implode(", ", $file_type);
                    }else{
                        $file_type = __("Unsupported");
                    }

                    if(!empty($cloud_import)){
                        $cloud_import = implode(", ", $cloud_import);
                    }else{
                        $cloud_import = __("Unsupported");
                    }
                ?>
                    <div class="col-md-4 col-sm-12">
                        <div class="pricing-box pricing-box-curbed wow fadeIn" data-av-animation="bounceInUp" style="visibility: visible; animation-name: fadeIn; <?php _e( $key%3 == 0 ?"br-left":"" )?> <?php _e( $row->popular==1?"popular":"" )?>">
                            <div class="pricing-top gradiant-background">
                                    <h5 class="price-title"><?php _e($row->name)?></h5>
                                    <h2 class="price">$<?php _e( $row->price_monthly )?></h2>
                                    <h5><?php _e($row->description)?></h5>
                                    </div>
                                    <div class="price-body">
                                    <ul class="pricing-tables-position">
                                        <li class="pl-0 text-center">
                                            <?php
                                            $social_networks_allowed = 0;
                                            if(!empty($posts)){
                                                foreach ($posts as $value){
                                                    if( isset($row->permissions[ $value['id']."_enable" ]) ){
                                                        $social_networks_allowed++;
                                                    }
                                                } 
                                            }
                                            ?>

                                            <div class="text-info fw-6 fs-16"><?php _e( sprintf( sprintf(__("Add up to %s social accounts"),  __( $social_networks_allowed * $row->number_accounts ) ) ) )?></div>
                                            <div class="small"><?php _e( sprintf( sprintf(__("%s social account on each platform"),  __( $row->number_accounts ) ) ) )?> </div>
                                        </li>
                                    </ul>
                                    <?php if(!empty($posts)){?>
                                    <ul class="pricing-tables-position">
                                        <li class="title"><span><b><?php _e("Scheduling & Report")?></b></span></li>
                                        <?php foreach ($posts as $value): ?>
                                            <li class="<?php _e( isset($row->permissions[ $value['id']."_enable" ]) ? "":"no" )?>"><?php _e( sprintf( sprintf(__("%s scheduling & report"),  __( $value['group'] ) ) ) )?></li>
                                        <?php endforeach ?>
                                    </ul>
                                    <?php }?>
                                    <?php if(!empty($addons)){?>
                                    <ul class="pricing-tables-position">
                                        <li class="title"><span><b><?php _e("Modules & Addons")?></b></span></li>
                                        <?php foreach ($addons as $value): ?>
                                            <li class="<?php _e( isset($row->permissions[ $value['id']."_enable" ]) ? "":"no" )?>"><?php _e( $value['sub_name'] )?></li>
                                        <?php endforeach ?>
                                    </ul>
                                    <?php }?>
                                    <ul class="pricing-tables-position">
                                        <li class="title"><span><b><?php _e("Advance features")?></b></span></li>
                                        <li class="have">Spintax support</li>
                                        <li class="<?php _e( isset($row->permissions[ "watermark_enable" ]) ? "":"no" )?>"><?php _e("Watermark support")?></li>
                                        <li class="<?php _e( isset($row->permissions[ "file_manager_image_editor" ]) ? "":"no" )?>"><?php _e("Image Editor support")?></li>
                                        <li class="have"><?php _e( sprintf( __( "Cloud import: %s"), $cloud_import ) )?></li>
                                        <li class="have"><?php _e( sprintf( __( "File type: %s"), $file_type ) )?></li>
                                        <li class="have"><?php _e( sprintf( __( "Storage: %sMB"), $row->permissions['max_storage_size'] ) )?></li>
                                        <li class="have"><?php _e( sprintf( __( "Max. file size: %sMB"), $row->permissions['max_file_size'] ) )?></li>                            
                                    </ul>
                                    <div class="pricing-tables-position-buton">
                                    <a href="<?php _e( get_url("payment/index/".$row->ids."/1" ))?>" data-tmp="<?php _e( get_url("payment/index/".$row->ids."/2" ))?>" class="button button-uppercase"><?php _e("Get Started")?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php }?>
