		<!-- Start .footer-section  -->
		<div class="footer-section section">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-12">
						<ul class="footer-navigation inline-list">
							<li class="nav-item">
								<a href="<?php _e( get_url() )?>"><?php _e("Home")?>
									</a>
								</li>
								<li class="nav-item">
									<a href="<?php _e( get_url() )?>"><?php _e("privacy_policy")?>	
									</a>
								</li>
				      	        <li class="nav-item">
				      	        	<a href="<?php _e( get_url() )?>"><?php _e("terms_of_services")?>
				      	        	</a>
				      	        </li>
								<li class="nav-item">
									<a href="<?php _e( get_url() )?>blog"><?php _e("Blog")?>
									</a>
								</li>
                                <li class="nav-item">
                                	<a href="<?php _e( get_url() )?>#faq"><?php _e("F.A.Q")?>
                                	</a>
                                </li>
						</ul>
						<ul class="social-list inline-list">
						<ul class="social-list">
                        <?php if(get_option("social_page_facebook", "") != ""){?>
                        <li class="list-inline-item">
                            <a href="<?=get_option("social_page_facebook", "")?>">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>
                        <?php }?>
                        <?php if(get_option("social_page_google", "") != ""){?>
                        <li class="list-inline-item">
                            <a href="<?=get_option("social_page_google", "")?>">
                                <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php }?>
                        <?php if(get_option("social_page_twitter", "") != ""){?>
                        <li class="list-inline-item">
                            <a href="<?=get_option("social_page_twitter", "")?>">
                                <i class="fa fa-twitter-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php }?>
                        <?php if(get_option("social_page_instagram", "") != ""){?>
                        <li class="list-inline-item">
                            <a href="<?=get_option("social_page_instagram", "")?>">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php }?>
                        <?php if(get_option("social_page_pinterest", "") != ""){?>
                        <li class="list-inline-item">
                            <a href="<?=get_option("social_page_pinterest", "")?>">
                                <i class="fa fa-pinterest-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php }?>
						<ul class="footer-links inline-list">
							<li>Copyright © 2020 S M Convenience</li>
						</ul>
					</div><!-- .col  -->
				</div><!-- .row  -->
			</div><!-- .container  -->
		</div><!-- .footer-section  -->
				
		<!-- JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="<?php _e( get_theme_frontend_url('assets/plus/assets/js/jquery.bundle.js'))?>"></script>
		<script src="<?php _e( get_theme_frontend_url('assets/plus/assets/js/script.js'))?>"></script>
	</body>
</html>