<?php include 'h-light.php'?>
			
			<div class="header-content pt-90">
				<div class="container">
					<div class="row text-center">
						<div class="col-md-8 col-md-offset-2">
							<div class="header-texts">
								<h1 class="cd-headline clip is-full-width wow fadeInUp" data-wow-duration=".5s">
									<span>Social Media </span> 
									<span class="cd-words-wrapper">
										<b class="is-visible">Design</b>
										<b>Posting</b>
										<b>Marketing</b>
									</span>
								</h1>
								<ul class="buttons">
								<?php if(!_s("uid")){?>
                                <?php if( get_option("signup_status", 1) ){?>
                                <a href="<?php _e( get_url("signup") )?>" class="buttonlogin wow fadeInUp"><?php _e("Free 30 day trial")?></a> or
                                <a href="<?php _e( get_url("login") )?>" class="button button-border button-transparent wow fadeInUp"><?php _e("Login")?></a>
                                <?php }else{?>
                                <a href="<?php _e( get_url("login") )?>" class="button button-border button-transparent wow fadeInUp"><?php _e("Login")?></a>
                                <?php }?>
                                <?php }else{?>
                                <a href="<?php _e( get_url("dashboard") )?>" class="button wow fadeInUp"><?php _e( sprintf( __("Hello, %s"), _u("fullname") ) )?></a>
                                <?php }?>
								</ul>
							</div>
						</div><!-- .col -->
					</div><!-- .row -->
					<div class="row text-center">
						<div class="col-md-10 col-md-offset-1">
							<div class="header-mockups">
								<div class="header-laptop-mockup black wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" >
									
								</div>
								<div class="iphonex-flat-mockup wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s">
								
								</div>
							</div>
						</div>
					</div>
				</div><!-- .container -->
			</div><!-- .header-content -->
		</div><!-- .header-section -->
		
		
		<!-- Start .about-section  -->
		<div id="about" class="about-section section white-bg">
			<div class="container tab-fix">
				<div class="section-head text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h2 class="heading"><span>Helping You to Bring More Customers!</span></h2>
							
						</div>
					</div>
				</div><!-- .section-head -->
				<div class="row tab-center mobile-center">
					<div class="col-md-6">
						<div class="video wow fadeInLeft" data-wow-duration=".5s">
							<img src="inc/themes/frontend/light/assets/plus/images/offers1.png" alt="about-video" />
							
						</div>
					</div><!-- .col -->
					<div class="col-md-6">
						<div class="txt-entry">
							<h3></h3>
							<p><b>One-Stop Connection for all your Brand Social Media Page.  Social Media Convenience is a Social media marketing company, providing solutions to convenience stores, liquor stores, etc. for all your brand needs and engagements.</b></p>
                            <p><b>We help a business to grow and succeed by leveraging social media and Advertisement.</p>
                            <p><b>Specializing in the design and development of social media marketing for all type of business, SMC has a proactive approach to solving problems and generates more leads.We are new in a market and making a difference; in the way, we go about work and execute projects.</b></p>
                            <p><b>At SMC, we have a penchant for studying business processes, delving on ideas, being creative, encourage innovation, adapt to new technologies, listen to people and finally understand the real problem.</b></p>
						
							<a href="#" class="button">Sign Up for Free</a>
						</div>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .about-section  -->
		
		
	
	
		
		
		<!-- Start .features-section  -->
		<div class="features-section section gradiant-background section-overflow-fix">
			<div class="container tab-fix" style="
    height: 800px;>
				<div class="features-content pt-10">
					<div class="row">
						<div class="col-md-7">
							<div class="section-head heading-light mobile-center tab-center">
								<div class="row">
									<div class="col-md-12">
										<h2 class="heading heading-light">We take social media off your daily “to-do” list so you can focus on doing what you do best – grow your business.</h2>
										<p><b>Social media content, done for you, every single day.
We know it’s not always easy to establish and maintain the social media presence for your small business. In fact, it can be challenging to find the hours or the know-how to make it happen.That’s where we come in.</p></b>

<p><b>SMC's social media content management to help busy entrepreneurs like you maintain a solid presence on social media, so that when your customers and potential customers go looking for you, you’re there. And you’re not just there, you’re there consistently, establishing your relevance, credibility, and approachability as a small business owner.</b></p>
									</div>
								</div>
							</div><!-- .section-head -->
							
						</div><!-- .col -->
						<div class="col-md-5 pt-100 text-center">
							<div class="feature-mockups clearfix">
								<div class="fearures-software-mockup black right wow fadeInUp" data-wow-duration=".5s"  data-wow-delay=".7s">
									<img src="inc/themes/frontend/light/assets/plus/images/home12.png" alt="software-screen"/>
								</div>
							
							</div>
						</div><!-- .col -->
					</div><!-- .row -->
				</div><!-- .features-content -->
			</div><!-- .container -->
		</div><!-- .features-section  -->
		
		
		<!-- Start .steps-section  -->
		<div class="section steps-section-alt pb-90 white-bg">
			<div class="container">
				<div class="section-head text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h2 class="heading"><span>Getting started is easy.</span></h2>
							
						</div>
					</div>
				</div><!-- .section-head  -->
				<div class="row text-center">
					<div class="col-md-12">
						<ul class="nav nav-tabs inline-nav text-center">
							<li class="active" data-toggle="tab" data-target="#tab1">
								<div class="steps"><h4>Get Started - Choose package</h4></div>
							</li>
						
							<li data-toggle="tab" data-target="#tab3">
								<div class="steps"><h4>Let us know abour your business</h4></div>
							</li>
							<li data-toggle="tab" data-target="#tab4">
								<div class="steps"><h4>Let the posting begin! Sit back and enjoy</h4></div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<div class="tab-content no-pd">
							<div class="tab-pane fade in active" id="tab1">
								<P><b>Click on any Get Started button to begin the process. First, you’ll create an account, then you’ll enter your payment details on a secure page and complete your order. Check your inbox right away for your Getting Started email.</b></P>
							</div>
							
							<div class="tab-pane fade" id="tab3">
																<P><b>Next, you’ll provide details about your business, products and services, and other information that will help us get to know you. You’ll also be asked to provide us with access to your social media accounts so we can start posting.</b></P>

							</div>
							<div class="tab-pane fade" id="tab4">
																<P><b>Here’s where the rubber meets the road. Within 2-3 business days of gaining access to your social media accounts, we’ll begin posting content to your pages. Then you’ll be free to focus on the parts of your business that need you the most.</b></P>

							</div>
						</div>
					</div>
				</div><!-- .row  -->
			</div><!-- .container  -->
		</div><!-- Start .statistics-section  -->
		
		
		<!-- Start .pricing-section  -->
		    <div id="pricing" class="getapp-section section gradiant-background">
		       	<div class="imagebg">
		       		<img src="inc/themes/frontend/light/assets/plus/images/pricing-bg.jpg" alt="pricing-bg">
		        	</div>
		        	<div class="gradiant-background gradiant-overlay gradiant-light"></div><!-- .gradiant-background  /exta div for transparent gradiant overlay /  -->
		        	<div class="container tab-fix">
		        		<div class="section-head heading-light text-center">
		        			<div class="row">
		        				<div class="col-md-8 col-md-offset-2">
		        				<h2 class="heading heading-light">Our Plans</h2>
		        				<p><b>Quality Content Every Single Day at a Price You Can Afford.</b></p>
		        				</div>
		        			</div>
		        		</div><!-- .col -->
				     <!-- Pricing table -->

                    <!--Pricing-->
                    <?php include 'p-light.php'?>
                    <!--Pricing End-->

					</div><!-- .col  -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .pricing-section  -->
		
		
		<!-- Start .faq-section  -->
		<div id="faq" class="faq-section section white-bg pt-120 pb-100">
			<div class="container">
				<div class="faq-alt">
					<div class="row tab-fix">
						<div class="col-md-4 tab-center mobile-center col-md-offset-1">
							<div class="side-heading">
								<h2 class="heading">SMC <span>FAQ</span></h2>
								<p>We got you coverd, check those faq if its not there just <a class="nav-item" href="#contacts">ask</a> us.</p>
							</div>
						</div><!-- .col  -->
						<div class="col-md-6">
							<!-- Accordion -->
							<div class="panel-group accordion" id="another" role="tablist" aria-multiselectable="true">
								<!-- each panel for accordion -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="accordion-i1">
										<h6 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#another" href="#accordion-pane-i1" aria-expanded="false">
												Is this app free to use for business or commercial use ?
												<span class="plus-minus"><span></span></span>
											</a>
										</h6>
									</div>
									<div id="accordion-pane-i1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="accordion-i1">
										<div class="panel-body">
											  <p>Internal audit is an independent, objective assurance and consulting activity designed to add value and improve an organization an independent, objective assurance and consulting activity.</p>
										</div>
									</div>
								</div> 
								<!-- each panel for accordion -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="accordion-i2">
										<h6 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#another" href="#accordion-pane-i2" aria-expanded="false">
												How do i make a support request with this app?
												<span class="plus-minus"><span></span></span>
											</a>
										</h6>
									</div>
									<div id="accordion-pane-i2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-i2">
										<div class="panel-body">
											  <p>Internal audit is an independent, objective assurance and consulting activity designed to add value and improve an organization an independent, objective assurance and consulting activity.</p>
										</div>
									</div>
								</div>
								<!-- each panel for accordion -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="accordion-i3">
										<h6 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#another" href="#accordion-pane-i3" aria-expanded="false">
												How and where can we download latest update ?
												<span class="plus-minus"><span></span></span>
											</a>
										</h6>
									</div>
									<div id="accordion-pane-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-i3">
										<div class="panel-body">
											  <p>Internal audit is an independent, objective assurance and consulting activity designed to add value and improve an organization an independent, objective assurance and consulting activity.</p>
										</div>
									</div>
								</div>
								<!-- each panel for accordion -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="accordion-i4">
										<h6 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#another" href="#accordion-pane-i4" aria-expanded="false">
												Is there any premium version with extended features ?
												<span class="plus-minus"><span></span></span>
											</a>
										</h6>
									</div>
									<div id="accordion-pane-i4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-i4">
										<div class="panel-body">
											  <p>Internal audit is an independent, objective assurance and consulting activity designed to add value and improve an organization an independent, objective assurance and consulting activity.</p>
										</div>
									</div>
								</div><!-- end each panel -->
								<!-- each panel for accordion -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="accordion-i5">
										<h6 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#another" href="#accordion-pane-i5" aria-expanded="false">
												Where do i find any details documentation ?
												<span class="plus-minus"><span></span></span>
											</a>
										</h6>
									</div>
									<div id="accordion-pane-i5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-i5">
										<div class="panel-body">
											  <p>Internal audit is an independent, objective assurance and consulting activity designed to add value and improve an organization an independent, objective assurance and consulting activity.</p>
										</div>
									</div>
								</div><!-- end each panel -->
								<!-- each panel for accordion -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="accordion-i6">
										<h6 class="panel-title">
											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#another" href="#accordion-pane-i6" aria-expanded="false">
												Are you gays aviable for making custom apps ?
												<span class="plus-minus"><span></span></span>
											</a>
										</h6>
									</div>
									<div id="accordion-pane-i6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-i6">
										<div class="panel-body">
											  <p>Internal audit is an independent, objective assurance and consulting activity designed to add value and improve an organization an independent, objective assurance and consulting activity.</p>
										</div>
									</div>
								</div><!-- end each panel -->
							</div><!-- Accordion #end -->
						</div><!-- .col  -->
					</div><!-- .row  -->
				</div><!-- .faq  -->
			</div><!-- .container  -->
		</div><!-- .faq-section  -->		
		
		<!-- Start .get-section  -->
		<div id="getapp" class="getapp-section section gradiant-background">
			<div id="contacts" class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="download-form pt-60 pb-40 pbm-0">
							<div class="heading-light">
								<h1 class="heading heading-light">Contact Us</h1>
								
							</div>
							<p></p>
							<p></p>
							<h2 class="heading heading-light">+1 847-582-0682</h2>
                            <h5 class="heading heading-light">Mon-Fri 9am-6pm</h5>
						</div>
					</div><!-- .col  -->
					<div class="col-md-5 col-md-offset-1 pt-120">
						<div class="browser-screen">
						    <p>
						 
						    </p>
						    <p></p>
						    <p></p>
						<h2 class="heading heading-light">Sales@smctool.com</h2>
                            <h5 class="heading heading-light">Online Sales</h5>
						</div>
					</div><!-- .col  -->
				</div><!-- .row  -->
			</div><!-- .container  -->
		</div><!-- .contact-section  -->
				
<?php include 'f-light.php'?>