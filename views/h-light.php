<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php _e( get_option('website_title', 'Stackposts - Social Marketing Tool') )?></title>
        <meta name="description" content="<?php _e( get_option('website_desc', '#1 Marketing Platform for Social Network') )?>">
        <meta name="keywords" content="<?php _e( get_option('website_keywords', 'social network, marketing, brands, businesses, agencies, individuals') )?>">
        <link rel="icon" type="image/png" href="<?php _e( get_option('website_favicon', get_url("inc/themes/backend/default/assets/img/favicon.png")) )?>" />

		<!-- Vendor Bundle CSS -->
		<link rel="stylesheet" href="<?php _e( get_theme_frontend_url('assets/plus/assets/css/vendor.bundle.css')) ?>">
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="<?php _e( get_theme_frontend_url('assets/plus/assets/css/style.css')) ?>">
		<link rel="stylesheet" href="<?php _e( get_theme_frontend_url('assets/plus/assets/css/theme-purple.css')) ?>">
		<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans">

		<script type="text/javascript">
            var token = '<?php _e( $this->security->get_csrf_hash() )?>',
                PATH  = '<?php _e(PATH)?>',
                BASE  = '<?php _e(BASE)?>';

            document.onreadystatechange = function () {
                var state = document.readyState
                if (state == 'complete') {
                    setTimeout(function(){
                        document.getElementById('interactive');
                        document.getElementById('loading-overplay').style.opacity ="0";
                    },500);

                    setTimeout(function(){
                        document.getElementById('loading-overplay').style.display ="none";
                        document.getElementById('loading-overplay').style.opacity ="1";
                    },1000);
                }
            }
        </script>
        <?php _e( htmlspecialchars_decode(get_option('embed_code', ''), ENT_QUOTES) , false)?>
	</head>
	<body>
       	
		<!-- Start .header-section -->
		<div id="home" class="header-section flex-box-middle section gradiant-background header-curbed-circle background-circles header-software">
			<div id="particles-js" class="particles-container"></div>
			<div id="navigation" class="navigation is-transparent" data-spy="affix" data-offset-top="5">
				<nav class="navbar navbar-default">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-collapse-nav" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="<?php _e( get_url() )?>">
                            <img alt="Logo" class="h-40" src="<?php _e( get_option('website_white', get_url("inc/themes/backend/default/assets/img/logo-white.png")) )?>" width="200" alt="brand-logo"></a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse font-secondary" id="site-collapse-nav">
							<ul class="nav nav-list navbar-nav navbar-right">
								<li class="nav-item"><a href="<?php _e( get_url() )?>"><?php _e("Home")?></a></li>
								<li class="nav-item"><a href="<?php _e( get_url() )?>#features"><?php _e("Features")?></a></li>
								<li class="nav-item"><a href="<?php _e( get_url() )?>#pricing"><?php _e("Pricing")?></a></li>
								<li class="nav-item"><a href="<?php _e( get_url() )?>blog"><?php _e("Blog")?></a></li>
                                <li class="nav-item"><a href="<?php _e( get_url() )?>#faq"><?php _e("F.A.Q")?></a></li>
                                <span class="nav-item">
                                <?php if(!_s("uid")){?>
                                <?php if( get_option("signup_status", 1) ){?>
                                <a href="<?php _e( get_url("login") )?>" class="buttonlogin"><?php _e("Login")?></a>
                                <a href="<?php _e( get_url("signup") )?>" class="buttonlogin wow fadeInUp"><?php _e("Sign Up")?></a>
                                <?php }else{?>
                                <a href="<?php _e( get_url("login") )?>" class="buttonlogin"><?php _e("Login")?></a>
                                <?php }?>
                                <?php }else{?>
                                <a href="<?php _e( get_url("dashboard") )?>" class="button wow fadeInUp"><?php _e( sprintf( __("Hello, %s"), _u("fullname") ) )?></a>
                                <?php }?>
			      	            </span>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container -->
				</nav>
			</div><!-- .navigation -->